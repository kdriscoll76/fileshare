<?php
session_start();
if(empty($_SESSION['username'])){ header("location:index.php");}
$username = $_SESSION['username'];

require("header.html");
require("config.php");
include("functions.php");

if(!empty($_GET['root'])){
 $root = $_GET['root'];
 $path = explode("/",$root);
 $depth = count($path);
 foreach($path as $trail){
 $count++;
  if($count <=  $depth ){
   $back .= "$trail";
   if($back == '/$dir/'){
    $back_url .= "$trail";
    $back_url .= "<a href='main.php?opt=repo&root=$back'>$trail/</a>";
   }else{
    $back_url .= "<a href='main.php?opt=repo&root=$back'>$trail/</a>";
   }
  }
 }

 if(!preg_match("/^\$dir.$/",$root)){
   $path = "<label>$back_url</label>";
 }else{
   $path = "<label>$dir</label>";
 }
}else{
 $root = $dir;
   $path =  "<label>$dir</label>";
}

//--header
echo"
<nav class='nav navbar navbar-inverse'>
<div class='navbar-text pull-right'><div class='btn-group'><button class='btn btn-primary'>$username</button><a class='btn btn-danger' href='logout.php'>x</a></div></div>
<div class='navbar-brand'>$sitename
</div>
<div class='container col-xs-12'>
</nav>
";
//--display area
echo "<div class='container'>
<div class='panel panel-info'>
<div class='panel-heading'>
<label>PATH: </label><label>$path</label>
</div>
<div class='view panel-body'>
<table id='table1' class='table'>
<thead>
<tr>
<th>TYPE</th>
<th>NAME</th>
<th>SIZE</th>
</tr>
</thead>
<tbody>";

$dirs = array_filter(glob("$root/*"), 'is_dir');
$files = array_filter(glob("$root/*"), 'is_file');
  foreach($files as $file){
  $size = format_filesize(filesize("$file"));
   $file = preg_replace("/^.*\//","",$file);
   if(preg_match("/\.(pdf)/i",$file)){
      $icon = 'pdf.png';
   }elseif(preg_match("/\.(doc?)/i",$file)){
      $icon = 'word.png';
   }elseif(preg_match("/\.(zip|tar)/i",$file)){
      $icon = 'compressed.png';
   }elseif(preg_match("/\.(xls?)/i",$file)){
      $icon = 'excel.png';
   }elseif(preg_match("/\.(ppt?)/i",$file)){
      $icon = 'powerpoint.png';
   }elseif(preg_match("/\.(sh|pl|exe|msi)/i",$file)){
      $icon = 'script.png';
   }elseif(preg_match("/\.(jpg|png|gif)/i",$file)){
      $icon = 'camera.svg';
   }elseif(preg_match("/\.(wmv|mp4|avi)/i",$file)){
      $icon = 'film.png';
   }elseif(preg_match("/\.(rdp)/i",$file)){
      $icon = 'remote.png';
   }elseif(preg_match("/\.(cert|pem)/i",$file)){
      $icon = 'cert.png';
   }else{
      $icon = 'text-document-icon.png';
   }
   $location = str_replace("$dir/",'share/',$root);
   echo"<tr>
   <td class='col-xs-2'>
   <a href='$location/$file' target='_blank'>
   <img style='vertical-align:middle;width:25px;'src='images/$icon'>
   </a>
   </td>
   <td class='col-xs-8'>$file</td>
   <td>$size</td>
   </tr>";
     $matches++;
}
foreach($dirs as $dir){
 $count++;
 $size = filesize("$dir");
 if($count <= 10){
 $dir = str_replace("//","/",$dir);
if(!preg_match("/$ignore/",$dir)){
 $folder = preg_replace("/^.*\//","",$dir);
 if( $root == $dir ){
echo "<tr>
<td class='col-xs-2'>
<a href='main.php?opt=repo&root=$dir'>
<img style='width:25px;' src='images/blue-folder.svg'/>
</a>
</td>
<td class='col-xs-10'>$folder</td>
</tr>";
 }else{
echo "<tr>
<td class='col-xs-2'>
<a href='main.php?opt=repo&root=$dir'>
<img style='vertical-align:middle;width:25px;' src='images/blue-folder.svg'/>
</a>
</td>
<td class='col-xs-8'>$folder</td>
<td>#</td>
</tr>";
}
}
}
$matches++;
}
?>
</tbody>
</table>
</div>
</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $("#table1").DataTable();
});
</script>