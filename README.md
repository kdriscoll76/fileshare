<h1>FILESHARE</h1>
<pre>
This application allows the sharing of files quickly and easily via a website.

Prerequisites
•	PHP
•	APACHE
•	BOOTSTRAP
•	LINUX OS

Steps to install and use.
1.	Create or add a folder with data to share.
2.	Clone the GIT repo into /var/www/html.
3.	Edit the config.php file and add the root path to your data-source folder.
4.	Create sym-link to inside the files folder name share which points to your data-source folder.
5.	Add/Edit the JSON accounts folder located in the data folder.
</pre>
